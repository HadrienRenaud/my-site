import React from "react";
import Page from "../components/Page";
import Hero from "../components/BulmaHelpers/Hero";

function CV() {
    return (
        <Page title="CV - Hadrien Renaud">
            <Hero>
                <h1 className="title is-2">
                    A small subset of what I did
                </h1>
                <p className="subtitle">
                    Or how did I became such a great chocolate eater.
                </p>
            </Hero>

            <section className="section content">
                <h2 className="title is-4">
                    Education
                </h2>
                <div className="columns">
                    <div className="column is-2 has-text-right">
                        current<br/> Aug 2020
                    </div>
                    <div className="column">
                        <p>
                            <strong>MPRI</strong> (Parisian Master of Research in Computer Science)
                            <br/>
                            Theoretical computer science.
                        </p>
                        <p>
                            Courses include: Distributed Computing, Graph algorithms, Static interpretation, Functional programming
                        </p>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-2 has-text-right">
                        Aug 2020 <br/>Aug 2017
                    </div>
                    <div className="column">
                        <p>
                            <strong>École Polytechnique</strong>, Paris
                            <br/>
                            France's leading university of science and engineering.
                        </p>
                        <p>
                            Major in Computer Science. Minor in Maths.
                            <ul>
                                <li>
                                    Courses include: Compilation, Distributed Computing, Networks, Operating
                                    Systems, Cryptology
                                </li>
                                <li>
                                    Group project in informatics for Macroeconomics prediction (with BNP)
                                </li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-2 has-text-right">
                        Jul 2017 <br/>Sept 2015
                    </div>
                    <div className="column">
                        <p>
                            <strong>Classe préparatoire</strong>, Lycée Henri IV, Paris
                            <br/>
                            Intensive 2 year undergraduate program to prepare for engineering school selective
                            exams.
                        </p>
                        <p>
                            Major in mathematics. Minor in Physics and Computer Science.
                            <br/>
                            Group project<em>Neural networks for caracters recognition</em>
                        </p>
                    </div>
                </div>
            </section>

            <section className="section content">
                <h2 className="title is-4">
                    Work experience
                </h2>
                <div className="columns">
                    <div className="column is-2 has-text-right">
                        Aug 2019 <br/>June 2019
                    </div>
                    <div className="column">
                        <strong>Fullstack Web Developer at Sipios</strong>, Paris
                        <ul>
                            <li>
                                Developing web applications for clients in the banking and financial sector
                            </li>
                            <li>
                                Implemented a code generator to quickly launch web sites
                            </li>
                            <li>
                                Learnt how to use the agile methodology in a lean environment
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="columns">
                    <div className="column is-2 has-text-right">
                        June 2019 <br/>Sept 2018
                    </div>
                    <div className="column">
                        <strong>Reviewer at Lycée "Henri IV"</strong>, Paris
                        <br/>
                        Tutorials and mock jury for students preparing nationwide exams in maths.
                    </div>
                </div>


                <div className="columns">
                    <div className="column is-2 has-text-right">
                        Apr 2018 <br/>Oct 2017
                    </div>
                    <div className="column">
                        <p>
                            <strong>French Government Defense procurement and technology agency</strong>
                            <br/>
                            Developed a numerical simulation project to evaluate the risks of a soldier
                            wearing a personal protective equipment against chemical weapons.
                        <br/>
                            Gave expertise on scientific methods to test gas masks and their results.
                            Analyzed experiments.
                        </p>
                    </div>
                </div>

                <div className="columns">
                    <div className="column is-2 has-text-right">
                        July 2016
                    </div>
                    <div className="column">
                        Work placement at EDF, Paris
                    </div>
                </div>
            </section>
            <Hero>
                <p className="has-text-right">
                    <em>to continue ...</em>
                </p>
            </Hero>
        </Page>
    );
}

export default CV;
