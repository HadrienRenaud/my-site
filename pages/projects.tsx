import React, {ReactNode} from "react";
import Link from "next/link";
import Page from "../components/Page";
import Hero from "../components/BulmaHelpers/Hero";
import {EA, SP} from "../components/JSXUtils";
import {groupBy, range} from "../services/tsutils";

export interface Project {
    name: string
    description: ReactNode
    whatIDid: ReactNode
    image?: string
    homepage?: string
    gitRepository?: string
}

export const PROJECTS: Project[] = [
    {
        name: "Sigma",
        description: "Social network for students of the École polytechnique",
        whatIDid: <>
            I did the frontend part, using <EA href="https://reactjs.org/">React</EA> with typescript
            , <EA href="https://react.semantic-ui.com/">Semantic UI React</EA> for the CSS framework
            , <EA href="https://apollographql.com/">Apollo</EA> for GraphQL bindings...
        </>,
        image: "/logo_sigma.png",
        gitRepository: "https://gitlab.binets.fr/br/sigma-frontend",
        homepage: "https://sigma.frankiz.net",
    },
    {
        name: "Appel commun à la reconstruction",
        description: "A site for a petition that should have been big.",
        whatIDid: <>
            I mostly worked on the <EA
            href="https://www.djangoproject.com/">Django</EA> part, mostly on
            backend development.
        </>,
        image: "/appel_commun_a_la_reconstruction.png",
        homepage: "https://www.appel-commun-reconstruction.org/",
        gitRepository: "https://gitlab.com/maxbellec/appel-crises-web/"
    },
    {
        name: "QDJ - question of the day",
        description: <>
            Small entertainment site for École polytechnique students.
            It is a poll app, that asks a question a day, and gives points to user when they answer.
        </>,
        whatIDid: <>
            I developed the main features of the site, using{' '}
            <EA href="https://flask.palletsprojects.com/">Flask</EA> and <EA href="https://bulma.io/">Bulma</EA>.
        </>,
        homepage: "https://qdj.binets.fr/",
        gitRepository: "https://gitlab.binets.fr/br/qdj",
    },
    {
        name: "Distance oracle with 3-hopset",
        description: <>
            A project for<SP/>
            <EA href="https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-29-2">MPRI 2.29.2 - Graph Mining</EA>,
            which was to build an efficient distance oracle on a graph with pre-processing.
        </>,
        whatIDid: <>
            I built a very fast Rust lib to construct a 2-hop labeling scheme and a distance oracle based on this
            pre-processing. It uses<SP/>
            <code><EA href="https://docs.rs/petgraph/0.5.1/petgraph/">petgraph::GraphMap</EA></code>
            <SP/> as base graph construct and I tried to fully use Rust possibilities to optimize this code.
        </>,
        gitRepository: "https://gitlab.com/HadrienRenaud/3hopsets",
    },
]

function Projects() {
    return (
        <Page title="Projects by Hadrien Renaud">
            <Hero>
                <h1 className="title is-2">A <span className="has-text-success">cool</span>lection of my projects</h1>
                <p className="subtitle">
                    This list may not be exhaustive, nor correct.
                    If you found an error, please <Link href="/contact">contact me</Link>.
                </p>
            </Hero>

            <div className="section">
                {groupBy(3, PROJECTS).map((projects, psid) => (
                    <div className="tile is-ancestor is-12" key={psid}>
                        {projects.map((p, pid) => (
                            <div className="tile is-parent" key={pid}>
                                <div className="tile is-child card is-flex is-flex-direction-column">
                                    {p.image && (
                                        <div className="card-image">
                                            <figure className="image">
                                                <img src={p.image} alt="Placeholder image"
                                                     style={{objectFit: "cover", maxHeight: 200}}/>
                                            </figure>
                                        </div>
                                    )}
                                    <div className="card-content content is-flex-grow-1">
                                        <p className="title is-4">{p.name}</p>

                                        <p>{p.description}</p>
                                        <hr/>
                                        <p>{p.whatIDid}</p>
                                    </div>
                                    <footer className="card-footer content">
                                        {p.homepage && <EA href={p.homepage} className="card-footer-item">HomePage</EA>}
                                        {p.gitRepository &&
                                        <EA href={p.gitRepository} className="card-footer-item">View code</EA>}
                                    </footer>
                                </div>
                            </div>
                        ))}
                        {(psid == Math.floor(PROJECTS.length / 3)) &&
                        range(3 - (PROJECTS.length % 3)).map(( i) => (
                            <div className="tile" key={i}/>
                        ))}
                    </div>
                ))}
            </div>
        </Page>
    );
}

export default Projects;
