import React, {useState} from "react";
import Page from "../components/Page";
import Hero from "../components/BulmaHelpers/Hero";
import {EA, SP} from "../components/JSXUtils";
import copy from "copy-to-clipboard";
import {getMyEmail} from "../services/getMyEmail";

function Email() {
    const [copied, setCopied] = useState<number>(0);

    const onClick = () => {
        copy(getMyEmail());
        setCopied(i => i + 1);
        setTimeout(() => setCopied(i => i - 1), 5000);
    }

    return <p>
        Email: <a onClick={onClick}>hadrien [dot] renaud [at] polytechnique [dot] edu</a>
        {copied ? <span style={{marginLeft: 8}}>✓ copied to clipboard!</span> : <></>}
    </p>
}

function Contact() {
    return (
        <Page title="Contact Hadrien Renaud">
            <Hero>
                <h1 className="title is-2">Hadrien Renaud</h1>
            </Hero>
            <div className="section content">
                <Email/>
            </div>
            <div className="section content">
                <h3 className="title is-4">Useful links</h3>
                <p>
                    My diverse git* accounts : <EA href="https://gitlab.com/HadrienRenaud">Gitlab.com</EA>,<SP/>
                    <EA href="https://github.com/HadrienRenaud">GitHub</EA>, <SP/>
                    <EA href="https://gitlab.binets.fr/hadrien.renaud">Gitlab.binets.fr</EA>
                </p>
                <p>
                    <EA href="https://www.linkedin.com/in/hadrien-renaud-5b4bb214a/">Linkedin</EA>
                </p>
            </div>
        </Page>
    );
}

export default Contact;
