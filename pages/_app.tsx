import { AppProps } from 'next/app'
import '../public/app.css';

function App({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />
}

export default App;
