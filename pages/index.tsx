import Page from "../components/Page";
import React from "react";
import Hero from "../components/BulmaHelpers/Hero";
import Link from "next/link";

const usefulLinks = [
    {
        text: "My CV",
        url: "/cv"
    },
    {
        text: "My projects",
        url: "/projects"
    },
    {
        text: "My contact",
        url: "/contact"
    }
]

const IndexPage = () => (
    <Page title="Hi I'm Hadrien Renaud">
        <Hero>
            <h1 className="title is-1">Hi, I'm Hadrien</h1>
        </Hero>
        <Hero size="medium">
            <h4 className="title is-4">What can you find here ?</h4>
            <div className="level">
                {usefulLinks.map(({text, url}) => (
                    <Link href={url}>
                        <div className="level-item">
                            <div className="button is-white is-large is-fullwidth">
                                {text}
                            </div>
                        </div>
                    </Link>
                ))}
            </div>
        </Hero>
    </Page>
)

export default IndexPage
