export function getMyEmail(): string {
    return "hadrien"
        .concat(".renaud")
        .concat("_polytechnique.edu")
        .replace("_", "@");
}
