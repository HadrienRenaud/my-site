
export function generateArray<A>(n: number, fn: (i: number) => A): A[] {
    const result: A[] = new Array<A>(n);

    for (let i = 0; i < n; i++)
        result[i] = fn(i);

    return result;
}

export function groupBy<T>(n: number, elements: T[]): T[][] {
    const result: T[][] = generateArray(Math.ceil(elements.length / n), () => new Array<T>())

    for (let index = 0; index < elements.length; index++)
        result[Math.floor(index / n)]?.push(elements[index]);

    return result
}

export function range(size: number) {
    return [...Array(size).keys()];
}
