import React, {AnchorHTMLAttributes, DetailedHTMLProps} from "react";


export function EA (props: DetailedHTMLProps<AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>) {
    return <a {...props} target="_blank" rel="noreferrer noopener"/>;
}

export function NBSP () {
    return <>{"\u00A0"}</>
}

export function SP() {
    return <>{" "}</>
}
