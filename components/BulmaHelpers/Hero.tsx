import React, {ReactNode} from "react";

export interface HeroProps {
    size?: "small" | "medium" | "large"
    children: ReactNode
}

function Hero (props: HeroProps) {
    const size = props.size ? "is-" + props.size : "";

    return (
        <div className={"hero " + size}>
            <div className="hero-body">
                {props.children}
            </div>
        </div>
    );
}

export default Hero;
