import React, {ReactNode} from "react";
import Head from "next/head";
import {EA, SP} from "./JSXUtils";

export interface PageProps {
    children: ReactNode,
    title: string,
}

const headerHeight = 48;
const footerHeight = 168;

function Page(props: PageProps) {
    const {title, children} = props;

    return <>
        <Head>
            <title>{title}</title>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        </Head>
        <header className="header" style={{height: headerHeight}}>
            <nav className="navbar container">
                <div className="navbar-brand">
                    <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false"
                       data-target="navbarBasicExample">
                        <span aria-hidden="true"/>
                        <span aria-hidden="true"/>
                        <span aria-hidden="true"/>
                    </a>
                </div>
                <div className="navbar-menu navbar-end">
                    <a className="navbar-item" href="/">
                        Home
                    </a>
                    <a className="navbar-item" href="/cv">
                        CV
                    </a>
                    <a className="navbar-item" href="/projects">
                        Projects
                    </a>
                    <a className="navbar-item" href="/contact">
                        Contact
                    </a>
                </div>
            </nav>
        </header>
        <div style={{minHeight: `calc(100vh - ${headerHeight}px - ${footerHeight}px)`}} className="container">
            {children}
        </div>
        <footer className="footer">
            <div className="content has-text-centered">
                Hadrien Renaud, 2020, created with <EA href="https://nextjs.org">NextJS</EA> and <SP/>
                <EA href="http://bulma.io/">Bulma</EA>.
                See on <EA href="https://gitlab.com/HadrienRenaud/my-site/">Gitlab</EA>.
            </div>
        </footer>
    </>;
}

export default Page;
